<?php
/*
Plugin Name: Plugin Prueba
Description: Plugin para una prueba
Author: Marco
*/


function woocommerce_carrito() {
    $carritoNum = "";
    if ( class_exists( "WooCommerce" ) ) {
        $carritoNum = WC()->cart->get_cart_contents_count(); 
        $carritoProductos=WC()->cart->get_cart();        
        $carritoUrl   = WC()->cart->get_cart_url();                
        $carritoIcono = '<i class="fa fa-shopping-cart"></i>';
    }
    $html  = '<div id="carrito">';
    $html.='<a href="'.$carritoUrl.'">'.$carritoIcono.' (' . $carritoNum . ')</a>';
    $html.='<ul id="productosCarrito">';
    $total=0;
    foreach($carritoProductos as $producto => $values) { 
        $_product =  wc_get_product( $values['data']->get_id() );
        $html.='<li class="woocommerce-mini-cart-item mini_cart_item">';        
        $getProductDetail = wc_get_product( $values['product_id'] );
        $html.= $getProductDetail->get_image('thumbnail'); 
        $html.= "<b>".$_product->get_title() .'</b> /'; 
        $price = get_post_meta($values['product_id'] , '_price', true);
        $html.= $price." &euro;";       
       $html.='</li>';
       $total=$price+$total;
    }
    $html.='<li>Total: '.$total.'</li>';
    $html.='</ul>';
    $html.='</div>';    
    return $html;
}

function my_custom_js() {
    echo "<style>
    #productosCarrito{
        display: none;
    }    
    #carrito a:hover + #productosCarrito{
        display: block;
    }    
    </style>";
}

add_action('wp_head', 'my_custom_js');

add_shortcode( "carrito", "woocommerce_carrito" );
?>